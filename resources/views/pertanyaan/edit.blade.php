@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="title" placeholder="Masukkan Judul" name="judul" value="{{old('title', $pertanyaan->judul)}}">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Pertanyaan</label>
                    <input type="text" class="form-control" id="body" placeholder="Masukkan Pertanyaan" name="isi" value="{{old('body', $pertanyaan->isi)}}">
                    @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Kirim</button>
                </div>
              </form>
            </div>
</div>            
@endsection